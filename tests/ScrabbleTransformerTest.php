<?php /** @noinspection PhpExpressionResultUnusedInspection */

namespace NinjaCat\PhpEtlExercise\Tests;

use Exception;
use NinjaCat\PhpEtlExercise\ScrabbleTransformer;
use PHPUnit\Framework\TestCase;

class ScrabbleTransformerTest extends TestCase
{
    public function test_transform_should_map_a_scoring_table_in_old_format_to_new_format()
    {
        $transformer = new ScrabbleTransformer();

        $this->assertEquals(self::NEW_SCORING_TABLE, $transformer->transform(self::OLD_SCORING_TABLE));
    }

    public function test_transform_should_throw_an_exception_when_data_contains_duplicate_letter()
    {
        $transformer = new ScrabbleTransformer();
        $malFormed = self::OLD_SCORING_TABLE + [11 => ['A']];

        $this->expectException(Exception::class);
        $transformer->transform($malFormed);
    }

    const OLD_SCORING_TABLE = [
        1 => ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'],
        2 => ['D', 'G'],
        3 => ['B', 'C', 'M', 'P'],
        4 => ['F', 'H', 'V', 'W', 'Y'],
        5 => ['K'],
        8 => ['J', 'X'],
        10 => ['Q', 'Z']
    ];

    const NEW_SCORING_TABLE = [
        'a' => 1, 'e' => 1, 'i' => 1, 'o' => 1, 'u' => 1, 'l' => 1, 'n' => 1,
        'r' => 1, 's' => 1, 't' => 1, 'd' => 2, 'g' => 2, 'b' => 3, 'c' => 3,
        'm' => 3, 'p' => 3, 'f' => 4, 'h' => 4, 'v' => 4, 'w' => 4, 'y' => 4,
        'k' => 5, 'j' => 8, 'x' => 8, 'q' => 10, 'z' => 10
    ];
}