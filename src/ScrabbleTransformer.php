<?php
namespace NinjaCat\PhpEtlExercise;

class ScrabbleTransformer {
    public function transform(array $oldScoringTable): array
    {
        $newScoringTable = [];

        foreach ($oldScoringTable as $score => $letters) {
            foreach ($letters as $letter) {
                $newScoringTable[strtolower($letter)] = $score;
            }
        }

        return $newScoringTable;
    }
}